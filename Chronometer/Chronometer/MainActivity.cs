﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Chronometer
{
	[Activity (Label = "@string/chronometer", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		TimeSpan accumulatedTime = new TimeSpan();

		TextView timeView;
		Button startStopButton;
		Button resetButton;

		ISharedPreferences data;

		private Handler handler;

		bool running = false;
		DateTime lastTime;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);

			timeView = (TextView)FindViewById (Resource.Id.timeView);
			startStopButton = (Button)FindViewById (Resource.Id.startStopButton);
			resetButton = (Button)FindViewById (Resource.Id.resetButton);

			startStopButton.Click += (sender, eventArgs) => StartStop();
			resetButton.Click += (sender, eventArgs) => Reset();

			data = GetSharedPreferences ("counter", FileCreationMode.Private);

			handler = new Handler ();

			ShowCount ();
		}

		protected override void OnSaveInstanceState(Bundle outState) {
			base.OnSaveInstanceState (outState);

			outState.PutFloat ("count", (float)accumulatedTime.TotalMilliseconds);
			outState.PutBoolean ("running", running);
			outState.PutLong ("lastTime", lastTime.Ticks);
			ShowCount ();
		}

		protected override void OnRestoreInstanceState(Bundle savedInstanceState) {
			base.OnRestoreInstanceState (savedInstanceState);

			accumulatedTime = TimeSpan.FromMilliseconds(savedInstanceState.GetFloat ("count"));
			running = savedInstanceState.GetBoolean ("running");
			lastTime = new DateTime (savedInstanceState.GetLong("lastTime"));
			ShowCount ();

			Restore ();
		}

		protected override void OnPause() {
			base.OnPause ();
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutFloat ("count", (float)accumulatedTime.TotalMilliseconds);
			editor.PutBoolean ("running", running);
			editor.PutLong ("lastTime", lastTime.Ticks);
			editor.Commit ();
		}

		protected override void OnResume() {
			base.OnResume ();
			accumulatedTime = TimeSpan.FromMilliseconds(data.GetFloat ("count", 0));
			running = data.GetBoolean ("running", false);
			lastTime = new DateTime (data.GetLong("lastTime", 0));
			ShowCount ();

			Restore ();
		}

		private void Restore() {
			if (running) {
				startStopButton.Text = Resources.GetString (Resource.String.stop);
				GenerateDelayedTick ();
			} else {
				startStopButton.Text = Resources.GetString (Resource.String.start);
			}
		}

		private void ShowCount() {
			timeView.Text = accumulatedTime.Hours.ToString ("00") + ":" + accumulatedTime.Minutes.ToString ("00") + ":" + accumulatedTime.Seconds.ToString ("00") + "." + accumulatedTime.Milliseconds.ToString ("000");
		}

		private void Increment() {
			accumulatedTime += DateTime.UtcNow.Subtract(lastTime);
			lastTime = DateTime.UtcNow;
			ShowCount ();
		}

		private void Reset() {
			accumulatedTime = new TimeSpan ();
			running = false;
			startStopButton.Text = Resources.GetString (Resource.String.start);
			ShowCount ();
		}

		private void StartStop() {
			running = !running;
			lastTime = DateTime.UtcNow;
			Restore ();
		}

		private void GenerateDelayedTick() {
			handler.PostDelayed (OnTick, 50);
		}

		private void OnTick() {
			if (running) {
				Increment ();
				GenerateDelayedTick ();
			}
		}
	}
}


