﻿using System;
using Android.Widget;
using Android.App;
using Android.Views;
using System.Collections.Generic;

namespace Dictionary
{
	public class DictionaryAdapter : BaseAdapter<string> {
		string[] items;
		Activity context;

		public DictionaryAdapter(Activity context, string[] items) : base() {
			this.context = context;
			this.items = items;
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public override string this[int position] {  
			get { return items[position]; }
		}
		public override int Count {
			get { return items.Length; }
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;
			if (view == null)
				view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItemChecked, null);
			view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = items[position];
			return view;
		}
	}
}

