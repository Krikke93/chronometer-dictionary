﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Dictionary.services.aonaware.com;
using System.Collections.Generic;

namespace Dictionary
{
	[Activity (Label = "Dictionary",MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		DictService service = new DictService();

		IList<Definition> definitions = new List<Definition>();
		bool[] selectedDictionaries;
		Button lookUpButton;
		EditText editText;
		ListView listView;
		ShareActionProvider shareActionProvider;
		ISharedPreferences data;
		DefinitionsAdapter adapter;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Main);
			data = GetSharedPreferences ("dictionary", FileCreationMode.Private);

			selectedDictionaries = new bool[service.DictionaryList().Length];
			lookUpButton = (Button)FindViewById (Resource.Id.button);
			editText = (EditText)FindViewById (Resource.Id.editText);
			listView = (ListView)FindViewById (Resource.Id.List);

			lookUpButton.Click += (sender,e) => lookUp();
			bool[] selectedDics = Intent.GetBooleanArrayExtra("MyData") ?? null;
			if (selectedDics != null)
				selectedDictionaries = selectedDics;
			adapter = new DefinitionsAdapter(this, definitions);
			listView.Adapter = adapter;
		}

		private void lookUp(){
			definitions.Clear ();
			if (editText.Text.Length > 0) {
				for (int i = 0; i < selectedDictionaries.Length; i++) {
					if (selectedDictionaries [i]) {
						WordDefinition def = service.DefineInDict (service.DictionaryList () [i].Id, editText.Text);
						if (def.Definitions.Length > 0) {
							for(int j = 0; j < def.Definitions.Length; j++){
								Definition definition = new Definition () {
									dictionary = def.Definitions [j].Dictionary.Name,
									definition = def.Definitions [j].WordDefinition
								};
								definitions.Add (definition);
							}
						}
					}
				}
				setIntent ();
			} else {
				//Eventueel: errormessage
			}
			adapter.newList (definitions);
		}

		private void chooseDictionaries(){
			var activity2 = new Intent (this, typeof(DictionarySelectionActivity));
			activity2.PutExtra ("dictionaries",getAllDics());
			activity2.PutExtra("selectedDics",getSelectedDics());
			StartActivity (activity2);
		}

		private Bundle getAllDics(){
			Bundle b = new Bundle ();
			b.PutStringArrayList ("dictionaries", getDicNames());
			return b;
		}

		private IList<String> getDicNames(){
			IList<String> dics = new List<String> ();
			foreach (Dictionary.services.aonaware.com.Dictionary dic in service.DictionaryList()) {
				dics.Add (dic.Name);
			}
			return dics;
		}

		private Bundle getSelectedDics(){
			Bundle b = new Bundle ();
			b.PutBooleanArray ("selectedDics", selectedDictionaries);
			return b;
		}
			
		public override bool OnCreateOptionsMenu(IMenu menu) {
			base.OnCreateOptionsMenu (menu);
			MenuInflater inflater = MenuInflater;
			inflater.Inflate (Resource.Layout.menu, menu);
			var shareMenuItem = menu.FindItem (Resource.Id.shareMenuItem);   
			shareActionProvider = (ShareActionProvider)shareMenuItem.ActionProvider;

			return true;
		}

		 private void setIntent ()
		{  
			var intent = new Intent (Intent.ActionSend);
			intent.SetType("text/plain");    
			intent.PutExtra (Intent.ExtraText, getList());
			shareActionProvider.SetShareIntent (intent);
		}

		private string getList(){
			string buffer = "";
			foreach(Definition def in definitions){
				buffer += def.toString() + "\n";
			}	
			return buffer;
		}

		public override bool OnOptionsItemSelected(IMenuItem item) {
			switch (item.ItemId) {
			case Resource.Id.shareMenuItem:
				return true;
			case Resource.Id.help:
				chooseDictionaries ();
				return true;
			default:
				return base.OnOptionsItemSelected(item);
			}
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutBoolean ("isRunning", selectedDictionaries != null);
			Console.WriteLine ("OnPause");
			store ();
		}

		protected override void OnRestart(){
			base.OnRestart ();
			if(data.GetBoolean("isRunning",false)) take ();
			editText.Text =  data.GetString ("search", null);
			lookUp ();
		}

		protected override void OnStop(){
			base.OnStop ();
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutBoolean ("isRunning", selectedDictionaries != null);
			store ();
		}

		protected override void OnResume(){
			base.OnResume ();
			Console.WriteLine ("OnResume");
			if(data.GetBoolean("isRunning",false)) take ();
			lookUp ();
		}
			
		private void store(){
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutString ("search", editText.Text);
			editor.PutInt ("dictionaries_size", selectedDictionaries.Length);  	
			for (int i = 0; i < selectedDictionaries.Length; i++){
				editor.PutBoolean ("dictionaries_" + i, selectedDictionaries [i]); 
			}
			editor.Commit ();
		}

		private void take(){
			int size = data.GetInt("dictionaries_size", 0);
			bool[] arr = new bool[size];
			for(int i=0;i<size;i++)  
				arr[i] = data.GetBoolean("dictionaries_" + i, false);  
			selectedDictionaries = arr;
		}

	}
}


